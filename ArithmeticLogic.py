""" ArithmeticLogic - Arithmetic comparaison module for floating values.
"""
__author__ = "Jacques-Bernard Lekien"
__copyright__ = "Copyright (c) 2019"
__date__ = "2019/08/01"
__version__ = "1.0.0"

class ArithmeticLogic:
  __slots__ = [ '_precision', '_coef' ]

  def __init__( self, precision = None ):
    '''
    Define the arithmetic precision
    '''
    self._precision = precision
    if precision is None:
      return
    # precalculates the multiplier coefficient before truncation
    self._coef = 10**precision 

  def usePrecision( self, p = None):
    '''
    @param p: if not None desired accuracy, otherwise use the
      precision set when instantiating this class.
    @return the multiplier coefficient before truncation
    '''
    if p is None:
      if self._precision is None:
        return None
      else:
        return self._coef
    return 10**p

  def eq( self, a, b, p = None ):
    '''
    @param a: first value to be compared
    @param b: second value to be compared
    @param p: if not None desired accuracy, otherwise use the
      precision set when instantiating this class.
    @return True if a is equal to b
    '''
    coef = self.usePrecision( p )
    if coef is None:
        return a == b
    return int( a * coef ) == int( b * coef )

  def ne( self, a, b, p = None ):
    '''
    @param a: first value to be compared
    @param b: second value to be compared
    @param p: if not None desired accuracy, otherwise use the
      precision set when instantiating this class.
    @return True if a is not equal to b
    '''
    coef = self.usePrecision( p )
    if coef is None:
        return a != b
    return int( a * coef ) != int( b * coef )

  def lt( self, a, b, p = None ):
    '''
    @param a: first value to be compared
    @param b: second value to be compared
    @param p: if not None desired accuracy, otherwise use the
      precision set when instantiating this class.
    @return True if a is strict lower than b
    '''
    coef = self.usePrecision( p )
    if coef is None:
        return a < b
    return int( a * coef ) < int( b * coef )

  def le( self, a, b, p = None ):
    '''
    @param a: first value to be compared
    @param b: second value to be compared
    @param p: if not None desired accuracy, otherwise use the
      precision set when instantiating this class.
    @return True if a is lower equal than b
    '''
    coef = self.usePrecision( p )
    if coef is None:
      return a <= b
    return int( a * coef ) <= int( b * coef )

  def gt( self, a, b, p = None ):
    '''
    @param a: first value to be compared
    @param b: second value to be compared
    @param p: if not None desired accuracy, otherwise use the
      precision set when instantiating this class.
    @return True if a is strict greater than b
    '''
    coef = self.usePrecision( p )
    if coef is None:
      return a > b
    return int( a * coef ) > int( b * coef )

  def ge( self, a, b, p = None ):
    '''
    @param a: first value to be compared
    @param b: second value to be compared
    @param p: if not None desired accuracy, otherwise use the
      precision set when instantiating this class.
    @return True if a is greater equal than b
    '''
    coef = self.usePrecision( p )
    if coef is None:
        return a >= b
    return int( a * coef ) >= int( b * coef )

  def isNull( self, a, p = None ):
    '''
    @param a: value to be compared
    @param p: if not None desired accuracy, otherwise use the
      precision set when instantiating this class.
    @return True if a is null
    '''
    coef = self.usePrecision( p )
    if coef is None:
        return a == 0
    return int( a * coef ) == 0

  def between( self, deb, v, fin ):
    return infegal( deb, v ) and infegal( v, fin )
 
alu = ArithmeticLogic( 7 )

