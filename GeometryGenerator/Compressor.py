import vtk

#import math
#
#from GeometricCylinder import Cylinder
#
#from GeometricVector import Vector
#from GeometricSphere import Sphere
#from GeometricPlane import Plane
#from GeometricBox import Box
#from GeometricSegment import Segment
#
#from ProfileGenerator import ProfileGenerator


def maskCompressor(scalar, inCursor, scalars):
    if inCursor.IsMasked():
        return True, None # masked
    # Leaf
    if inCursor.IsLeaf():
        return False, scalars.GetComponent( inCursor.GetGlobalNodeIndex(), 0 )
    # Coarse
    for ichild in range( 0, inCursor.GetNumberOfChildren() ):
        inCursor.ToChild( ichild )
        rmasked, rscalar = maskCompressor(scalar, inCursor, scalars)
        if rmasked or scalar != rscalar:
            inCursor.ToParent()
            return False, None # not masked, differentes values
        inCursor.ToParent()
    return False, scalar # not masked, values constant

def handleCompressor(inCursor, outCursor, infields, outfields):
    # Add value in fields
    inidx = inCursor.GetGlobalNodeIndex()
    inscalar = infields['scalar'].GetComponent(inidx,0)
    outidx = outCursor.GetGlobalNodeIndex()
    outfields['scalar'].InsertTuple1(outidx, inscalar)
    outfields['level'].InsertTuple1(outidx, infields['level'].GetComponent(inidx,0))
    # If masked
    masked = inCursor.IsMasked()
    outCursor.SetMask( masked )
    if masked:
        return
    # Leaf
    if inCursor.IsLeaf():
        return
    #
    rmasked, rscalar = maskCompressor(inscalar, inCursor, infields['scalar'])    
    assert( not rmasked )
    #
    if rscalar is None:
        outCursor.SubdivideLeaf()
        for ichild in range( 0, inCursor.GetNumberOfChildren() ):
            inCursor.ToChild(ichild)
            outCursor.ToChild(ichild)
            handleCompressor(inCursor, outCursor, infields, outfields)
            inCursor.ToParent()
            outCursor.ToParent()

def Compressor(inhtg, nscalar, mode='sans perte'):
    '''
    nscalar: le nom du scalar qui permet la compression
    '''
    inScalarArray = inhtg.GetPointData().GetArray('scalar')
    assert(inScalarArray)
    print("## ", inScalarArray.GetNumberOfTuples())
    inLevelArray = inhtg.GetPointData().GetArray('level')
    assert(inLevelArray)
    print("## ", inLevelArray.GetNumberOfTuples())
    
    infields = {'level': inLevelArray, 'scalar': inScalarArray, }    
    #Scalar Level
    outLevelArray = vtk.vtkUnsignedCharArray()
    outLevelArray.SetName('level')
    outLevelArray.SetNumberOfTuples(0)

    outScalarArray = vtk.vtkDoubleArray()
    outScalarArray.SetName('scalar')
    outScalarArray.SetNumberOfTuples(0)
    
    outfields = {'level': outLevelArray, 'scalar': outScalarArray, }    
    # Create a cursor
    outhtg = vtk.vtkHyperTreeGrid()
    outhtg.SetDimensions( inhtg.GetDimensions() )
    outhtg.SetBranchFactor( inhtg.GetBranchFactor() )
    outhtg.SetXCoordinates( inhtg.GetXCoordinates() )
    outhtg.SetYCoordinates( inhtg.GetYCoordinates() )
    outhtg.SetZCoordinates( inhtg.GetZCoordinates() )
    
    inCursor = vtk.vtkHyperTreeGridNonOrientedCursor()
    outCursor = vtk.vtkHyperTreeGridNonOrientedCursor()
    # Index "global"
    outCrtIndex = 0
    for treeId in range(inhtg.GetMaxNumberOfTrees()):
        inhtg.InitializeNonOrientedCursor(inCursor, treeId)
        if inCursor.IsMasked():
            continue
        outhtg.InitializeNonOrientedCursor(outCursor, treeId, True)
        outCursor.SetGlobalIndexStart(outCrtIndex)
        #
        handleCompressor(inCursor, outCursor, infields, outfields)
        #
        outCrtIndex += outCursor.GetTree().GetNumberOfVertices()
        
    print("# ", outCrtIndex)
    
    #On dirait que le addarray invalide les tableaux
    #qui ne sont pas bien dimensionnes
    #Je le declarais avant quand taille 0 avec erreur lors de l'acces...
    #et le faire apres ca marche
    outhtg.GetPointData().AddArray(outLevelArray)
    outhtg.GetPointData().AddArray(outScalarArray)
    
    return outhtg

