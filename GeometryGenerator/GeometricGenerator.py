import vtk

import math

from GeometricCylinder import Cylinder

from GeometricVector import Vector
from GeometricSphere import Sphere
from GeometricPlane import Plane
from GeometricBox import Box
from GeometricSegment import Segment

from Compressor import Compressor

from ProfileGenerator import ProfileGenerator

TARGET_LEVEL = 6

def handleCell(geometricObjects, geoCursor, maskArray, fields):
    val = -1
    scalar = -1
    
    etat = None
    
    bounds = range(6)
    geoCursor.GetBounds( bounds ) 
    box = Box( bounds)    
    for gg in geometricObjects:
      crtVal = gg['geometry'].intersecte( box )
      if gg['mode'] == 1: # ADD
        if crtVal >= 0:
          val = crtVal
          scalar = gg['scalar']
          etat = 1
      elif gg['mode'] == 0: # REPLACE
        if crtVal >= 0:
          scalar = gg['scalar']
          etat = 0
      elif gg['mode'] == -1: # SUB
        if crtVal >= 0:
          if geoCursor.GetLevel() == TARGET_LEVEL:
            val = -1
          etat = -1
    # Add value in fields
    idx = geoCursor.GetGlobalNodeIndex()
    # In geometric object
    fields['scalar'].InsertTuple1(idx, scalar)
    fields['level'].InsertTuple1(idx, geoCursor.GetLevel())
    # Out geometric object
    if val < 0:
        return 1 # Masked
    # Unmasked
    maskArray.InsertTuple1(idx, False)
    # Leaf
    if geoCursor.IsLeaf():
        if geoCursor.GetLevel() >= TARGET_LEVEL:
            return
        geoCursor.SubdivideLeaf()
        # Default state for children
        geoCursor.ToChild(0)
        idx = geoCursor.GetGlobalNodeIndex()
        for ichild in range( 0, geoCursor.GetNumberOfChildren() ):
            maskArray.InsertTuple1(idx+ichild, True)
    else:
        geoCursor.ToChild(0)
    # Browse the children
    # Elder child
    handleCell(geometricObjects, geoCursor, maskArray, fields)
    geoCursor.ToParent()
    # And follow
    for ichild in range( 1, geoCursor.GetNumberOfChildren() ):
        geoCursor.ToChild(ichild)
        handleCell(geometricObjects, geoCursor, maskArray, fields)
        geoCursor.ToParent()

def GeometricGenerator(htg, geometricObjects):
    #Scalar Level
    levelArray = vtk.vtkUnsignedCharArray()
    levelArray.SetName('level')
    levelArray.SetNumberOfTuples(0)

    scalarArray = vtk.vtkDoubleArray()
    scalarArray.SetName('scalar')
    scalarArray.SetNumberOfTuples(0)
    
    fields = {'level': levelArray, 'scalar': scalarArray, }

    # Create a cursor
    geoCursor = vtk.vtkHyperTreeGridNonOrientedGeometryCursor()
    # Index "global"
    crtIndex = 0

    for treeId in range(htg.GetMaxNumberOfTrees()):
        htg.InitializeNonOrientedGeometryCursor(geoCursor, treeId, True)
        #
        geoCursor.SetGlobalIndexStart(crtIndex)
        htg.GetMask().InsertTuple1(geoCursor.GetGlobalNodeIndex(), True)
        #
        handleCell(geometricObjects, geoCursor, htg.GetMask(), fields)
        #
        crtIndex += geoCursor.GetTree().GetNumberOfVertices()
        
    print("# ", crtIndex)
    
    #On dirait que le addarray invalide les tableaux
    #qui ne sont pas bien dimensionnes
    #Je le declarais avant quand taille 0 avec erreur lors de l'acces...
    #et le faire apres ca marche
    htg.GetPointData().AddArray(levelArray)
    htg.GetPointData().AddArray(scalarArray)

#--------------------------------

#Generate a HyperTree Grid
htg = vtk.vtkHyperTreeGrid()

htg.Initialize()
htg.SetDimensions([7, 3, 3]) #GridPoints
htg.SetBranchFactor(2)

xValues = vtk.vtkDoubleArray() #x
xValues.SetNumberOfValues(7)
xValues.SetValue(0, -3)
xValues.SetValue(1, -2)
xValues.SetValue(2, -1)
xValues.SetValue(3,  0)
xValues.SetValue(4,  1)
xValues.SetValue(5,  2)
xValues.SetValue(6,  3)
htg.SetXCoordinates(xValues)

yValues = vtk.vtkDoubleArray() #y
yValues.SetNumberOfValues(3)
yValues.SetValue(0, -1)
yValues.SetValue(1,  0)
yValues.SetValue(2,  1)
htg.SetYCoordinates(yValues)

zValues = vtk.vtkDoubleArray() #z
zValues.SetNumberOfValues(3)
zValues.SetValue(0, -1)
zValues.SetValue(1,  0)
zValues.SetValue(2,  1)
htg.SetZCoordinates(zValues)

geometricObjects = [
    {   'geometry': Cylinder([0., 0., 0.], 0.75, 0.75, 0),
        'scalar': 1.,
        'mode': +1, # ADD
    },
    {   'geometry': Cylinder([0., 0., 0.], 0.5, 1., 0),
        'scalar': 2.,
        'mode': +1, # ADD
    },
    {   'geometry': Cylinder([0., 0., 0.], 0.4, 1., 0),
        'scalar': 3.,
        'mode': -1, # SUB
    },
]

geometricObjects = [
    {   'geometry': Sphere([-1., 0., 0.], 0.75),
        'scalar': 1.,
        'mode': +1, # ADD
    },
    {   'geometry': Sphere([-1., 0., 0.], 0.3),
        'scalar': -1.,
        'mode': -1, # SUB
    },
    {   'geometry': Sphere([ 1., 0., 0.], 0.75),
        'scalar': 2.,
        'mode': +1, # ADD
    },
    {   'geometry': Sphere([ 1., 0., 0.], 0.3),
        'scalar': -2.,
        'mode': -1, # SUB
    },
    {   'geometry': Sphere([ 0., 0., 0.], 1.),
        'scalar': -3.,
        'mode': -1, # SUB
    },
    {   'geometry': Plane(0.5,-0.5,0.,0.),
        'scalar': 4.,
        'mode': +1, # ADD
    },
    {   'geometry': Box([-1., 1., 0.51, 1., -1., 1.]),
        'scalar': 5.,
        'mode': -1, # SUB
    },
    {   'geometry': Cylinder([0., 0., 0.], 0.4, 1., 2),
        'scalar': 6.,
        'mode': +1, # ADD
    },
    {   'geometry': Cylinder([0., 0., 0.], 0.2, 1., 2),
        'scalar': -1.,
        'mode': -1, # SUB
    },
    {   'geometry': Cylinder([0., 0., 0.], 0.2, 2., 0),
        'scalar': -1.,
        'mode': -1, # SUB
    },
    #{   'geometry': Segment( Vector( [-2., 0.45, 0.05] ), Vector( [2., -0.45, 0.05] ) ),
    #    'scalar': 8.,
    #    'mode': 1, # ADD
    #},
]

geometricObjectsZ = [
    {   'geometry': Sphere([ 0., 0., 0. ], 1 ),
        'scalar': 1.,
        'color': [ 255, 0, 0 ],
        'mode': +1, # ADD
    },
    {   'geometry': Box([ -1., 1., -1., 1., -0.03, 0.03 ]),
        'scalar': -1.,
        'mode': -1, # SUB
    },
    {   'geometry': Sphere([ 0., 0., 0. ], 0.95 ),
        'scalar': 2.,
        'color': [ 0, 0, 0 ],
        'mode': +1, # ADD
    },
    {   'geometry': Sphere([ 1., 0., 0. ], 0.18 ),
        'scalar': 3.,
        'color': [ 0, 0, 0 ],
        'mode': 0, # REPLACE
    },
    {   'geometry': Sphere([ 1., 0., 0. ], 0.15 ),
        'scalar': 4.,
        'color': [ 0, 0, 0 ],
        'mode': 0, # REPLACE
    },
]

GeometricGenerator(htg, geometricObjects)

# htg = Compressor(htg, 'scalar')

# crb = ProfileGenerator(htg, Segment( Vector( [-2., 0.45, 0.05] ), Vector( [2., -0.45, 0.05] ) ))

if False: # For validation
    scalar = htg.GetPointData().GetArray('scalar')
    assert(scalar)
    print("## ", scalar.GetNumberOfTuples())
    level = htg.GetPointData().GetArray('level')
    assert(level)
    print("## ", level.GetNumberOfTuples())

# Depth Limiter Filter
print('With Depth Limiter Filter (HTG)')
depth = vtk.vtkHyperTreeGridDepthLimiter()
depth.SetInputData(htg)
depth.SetDepth(1024)

if False: # For validation
    print('htg:',htg.GetNumberOfVertices())
    pointData = htg.GetPointData()
    field = pointData.GetArray('level')
    print('Field: level')
    print('> nb: ', field.GetNumberOfTuples())
    print('>range: ', field.GetRange())
    field = pointData.GetArray('scalar')
    print('Field: scalar')
    print('> nb:', field.GetNumberOfTuples())
    print('>range: ', field.GetRange())

# BUG Le range n'est pas bon si on fixed le niveau max.

#Generate a polygonal representation of a hypertree grid
geometry = vtk.vtkHyperTreeGridGeometry()
# geometry.SetInputConnection(ex.GetOutputPort())
geometry.SetInputData(htg)

#Shrink this mesh
if False:
  shrink = vtk.vtkShrinkFilter()
  shrink.SetInputConnection(geometry.GetOutputPort())
  shrink.SetShrinkFactor(.8)
else:
  shrink = geometry
  
#Create a new window
renWin = vtk.vtkRenderWindow()
renWin.SetWindowName( "Generator HTG" )
renWin.SetSize( 1600, 800 )
renWin.SetSize( 800, 400 )

for nameField in ['level', 'scalar']:
  print(nameField)
  shrink.Update()
  dataRange = shrink.GetOutput().GetCellData().GetArray(nameField).GetRange()
  print('> dataRange:',dataRange)

  # LookupTable
  lut = vtk.vtkLookupTable()
  lut.SetHueRange(0.66, 0)
  lut.Build()

  #Create a mapper
  mapper = vtk.vtkDataSetMapper()
  mapper.SetInputConnection( shrink.GetOutputPort() )

  mapper.SetLookupTable(lut)
  mapper.SetColorModeToMapScalars()
  mapper.SetScalarModeToUseCellFieldData()
  mapper.SelectColorArray(nameField)
  mapper.SetScalarRange(dataRange[0], dataRange[1])

  #Connect the mapper to an actor
  actor = vtk.vtkActor()
  actor.SetMapper( mapper )

  #Create a renderer and add the actor to ir
  renderer = vtk.vtkRenderer()
  renderer.SetBackground( 0., 0., 0. )
  renderer.AddActor( actor )
  
  if nameField == 'level':
    renderer.SetViewport( 0., 0., 0.5, 1. ) #xmin,ymin,xmax,ymax
  else:
    renderer.SetViewport( 0.5, 0., 1., 1. ) #xmin,ymin,xmax,ymax
  
  renWin.AddRenderer( renderer )

#Create an interactor
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow( renWin )

#Initialize the interaction and start the rendering loop
iren.Initialize()
renWin.Render()
iren.Start()


