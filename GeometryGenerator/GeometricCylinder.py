from GeometricBox import Box

class Cylinder:
    '''
    Geometric Cylinder
    '''
    __slots__ = [ '_center', '_radius', '_radius2', '_halfsize', '_orientation', '_box' ]

    def __init__( self, center, radius, halfsize, orientation ):
        '''
        Constructor
        orientation: 0-x, 1-y, 2-z
        '''
        self._center = tuple(center)
        self._radius = radius
        self._radius2 = radius**2
        self._halfsize = halfsize
        self._orientation = orientation
        bounds = []
        for i in range(3):
            if orientation == i:
                bounds += [center[0]-halfsize, center[0]+halfsize]
            else:
                bounds += [center[0]-radius, center[0]+radius]
        self._box = Box(bounds)

    def intersecte( self, box ):
        '''
        Intersecte method with a align axis cell
        return -1, if not intersect
        return 0, if intersect but not in
        return 1, if in
        '''
        if self._box.speedIntersecte( box ) == -1:
            return -1
        if self._orientation == 0:
          if box.bounds[0] > self._center[0]+self._halfsize or box.bounds[1] < self._center[0]-self._halfsize:
            return -1
        elif self._orientation == 1:
          if box.bounds[2] > self._center[1]+self._halfsize or box.bounds[3] < self._center[1]-self._halfsize:
            return -1
        elif self._orientation == 2:
          if box.bounds[4] > self._center[2]+self._halfsize or box.bounds[5] < self._center[2]-self._halfsize:
            return -1
        dD = self._radius2            
        for i in range( 3 ):
            if self._orientation == i:
                continue
            if self._center[i] < box.bounds[2*i]:
                dD -= ( self._center[i] - box.bounds[ 2*i ] )**2
            elif self._center[i] > box.bounds[2*i+1]:
                dD -= ( self._center[i] - box.bounds[ 2*i+1 ] )**2
        if dD == 0.:
            return 0
        elif dD > 0:
            return 1
        return -1

