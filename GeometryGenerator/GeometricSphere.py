class Sphere:
    '''
    Geometric Sphere
    '''
    __slots__ = [ '_center', '_radius', '_radius2' ]

    def __init__( self, center, radius ):
        '''
        Constructor
        '''
        self._center = tuple(center)
        self._radius = radius
        self._radius2 = radius**2

    def __prune( self, box ):
        if box.bounds[0] > self._center[0]+self._radius or box.bounds[1] < self._center[0]-self._radius:
            return True
        if box.bounds[2] > self._center[1]+self._radius or box.bounds[3] < self._center[1]-self._radius:
            return True
        if box.bounds[4] > self._center[2]+self._radius or box.bounds[5] < self._center[2]-self._radius:
            return True
        return False

    def intersecte( self, box ):
        '''
        Intersecte method with a align axis cell
        return -1, if not intersect
        return 0, if intersect but not in
        return 1, if in
        '''
        if self.__prune( box ):
            return -1
        dD = self._radius2
        for i in range( 3 ):
            if self._center[i] < box.bounds[2*i]:
                dD -= ( self._center[i] - box.bounds[ 2*i ] )**2
            elif self._center[i] > box.bounds[2*i+1]:
                dD -= ( self._center[i] - box.bounds[ 2*i+1 ] )**2
        if dD == 0.:
            return 0
        elif dD > 0:
            return 1
        return -1

if __name__ == '__main__':
    print("> Test GeometricShpere in run")
    v = Sphere( [ 1., 2., 3. ], 2. )
    #
    from GeometricBox import Box
    box = Box( [ 0.75, 1.25, 1.75, 2.25, 2.75, 3.25 ] )
    #
    assert( intersecte( box ) == 1 )
    print("> Test GeometricShpere terminated")

