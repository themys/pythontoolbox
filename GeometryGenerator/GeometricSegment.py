from ArithmeticLogic import alu

from GeometricVector import Vector
from GeometricBox import Box

class Segment:
    '''
    Geometric Segment
    '''
    __slots__ = [ '_pd', '_pf', '_v', '_u' ]

    def __init__( self, pd, pf ):
        '''
        Constructor by point, vecteur
        '''
        assert( isinstance(pd, Vector) )
        assert( isinstance(pf, Vector) )
        self._pd = pd
        self._pf = pf
        self._v = None
        self._u = None
    
    @property
    def p(self):
        return self._pd
        
    @property
    def v( self ):
        if self._v is None:
            self._v = self._pf - self._pd
        return self._v
               
    @property
    def u( self ):
        if self._u is None:
            self._u = Vector( self.v.xyz )
            self._u.normalisation
        return self._u

    def projected( self, p ):
        return self.u * ( ( p - self.p ) * self.u ) + self.p

    def distance( self, p ):
        proj = self.projected( p )
        return proj.square_distance( p )
        
    def abscisse( self, p ):
        '''
        return un abs entre 0 et 1
        '''
        return self.v * ( p -self.p )

    def point( self, abscisse ):
        '''
        prend un abscisse entre 0 et 1
        '''
        return self.v * abscisse + self.p
    
    def included( self, p ):
        if not alu.nul( self.p.square_distance( p ) ):
            False
        absc = self.abscisse( p )
        if alu.between( 0, absc, 1 ):
            return True
        return False
    
    def linear( self, box ):
        assert( isinstance( box, Box ) )
        memo = []
        #
        if box.interieur( self._pd ) and box.interieur( self._pf ):
            return [ 0., 1. ]
        #
        # Optimisation possible, si segment petit alors intersection avec la sphere ?
        #
        for i in range(2):
          try:
            absc = ( box.bounds[i] - self.p.xyz[0] ) / self.v.xyz[0]
            if alu.between( 0., absc, 1. ):
              p = self.point( absc )
              if alu.eq( p.xyz[0], box.bounds[i] ):
                if alu.between( box.bounds[2], p.xyz[1], box.bounds[3] ):
                  if alu.between( box.bounds[4], p.xyz[2], box.bounds[5] ):
                    memo += [ absc ]
          except:
            pass
          #
          try:
            absc = ( box.bounds[2+i] - self.p.xyz[1] ) / self.v.xyz[1]
            if alu.between( 0., absc, 1. ):
              p = self.point( absc )
              if alu.eq( p.xyz[1], box.bounds[2+i] ):
                if alu.between( box.bounds[0], p.xyz[0], box.bounds[1] ):
                  if alu.between( box.bounds[4], p.xyz[2], box.bounds[5] ):
                    memo += [ absc ]
          except:
            pass
          #
          try:
            absc = ( box.bounds[4+i] - self.p.xyz[2] ) / self.v.xyz[2]
            if alu.between( 0., absc, 1. ):
              p = self.point( absc )
              if alu.eq( p.xyz[2], box.bounds[4+i] ):
                if alu.between( box.bounds[0], p.xyz[0], box.bounds[1] ):
                  if alu.between( box.bounds[3], p.xyz[1], box.bounds[4] ):
                    memo += [ absc ]
          except:
            pass
        #
        return memo
        
    def intersecte( self, box ):
        assert( isinstance( box, Box ) )
        
        memo = self.linear( box )
        
        if len( memo ) == 2 and alu.eq( memo[0], 0 ) and alu.eq( memo[1], 1 ):
            return 1
            
        if len( memo ) > 0:
            return 0
        return -1
        
