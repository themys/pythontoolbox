""" Vector - Vector arithmetic module, a point is a specific vector.
      Version with and without numpy.
"""
__author__ = "Jacques-Bernard Lekien"
__copyright__ = "Copyright (c) 2019"
__date__ = "2019/08/01"
__version__ = "1.0.0"

from ArithmeticLogic import alu

try:
  import numpy as np

  class Vector:
    '''
    '''
    __slots__ = [ '_xyz' ]

    def __init__( self, x, y = None, z = None ):
      '''
      '''
      if y is None and z is None:
        self._xyz = np.array( x )
      else:
        self._xyz = np.array( ( x, y, z ) )

    @property
    def xyz( self ):
      return self._xyz

    def dot( self, v ):
      '''
      
      produit scalaire equivalent a
      np.sum(self._xyz*v.xyz)
      '''
      return np.vdot( self._xyz, v.xyz )

    def cross( self, v ):
      '''
      produit vectorielle
      '''
      return Vector( np.cross( self._xyz, v.xyz ) )

    @property
    def norm( self ):
      '''
      norme equivalent
      math.sqrt(np.sum(self._xyz**2)))
      '''
      return np.linalg.norm( self._xyz )

    @property
    def normalisation( self ):
      '''
      '''
      self._xyz /= self.norm
      return self

    def square_distance( self, p ):
      u = p.xyz - self._xyz
      return np.sum( u._xyz**2 )

    def __add__(self, other):
      return Vector( self._xyz + other.xyz )

    def __iadd__(self, other):
      self._xyz = self._xyz + other.xyz
      return self

    def __sub__(self, other):
      return Vector( self._xyz - other.xyz )

    def __isub__(self, other):
      self._xyz = self._xyz - other.xyz
      return self
    
    def __mul__( self, other ):
      if isinstance( other, Vector ):
        return self.dot( other )
      return Vector( self._xyz * other )

    def __eq__( self, other ):
      return self._xyz == other.xyz
        
    def eq( self, other, p = None ):
      coef = alu.usePrecision( p )
      if coef is None:
        return self._xyz == other.xyz
      for i in range(3):
        if not alu.eq( self._xyz[i], other.xyz[i], p):
          return False
      return True

except ImportError:
  import math

  class Vector:
    '''
    '''
    __slots__ = [ '_xyz' ]

    def __init__( self, x, y = None, z = None ):
      '''
      '''
      if y is None and z is None:
        self._xyz = x
      else:
        self._xyz = (x, y, z)

    @property
    def xyz( self ):
      return self._xyz

    def dot( self, v ):
      '''
      produit scalaire equivalent a
      np.sum(self._xyz*v.xyz)
      '''
      sum = 0
      for i in range(3):
        sum += self._xyz[i] * v.xyz[i]
      return sum
  
    def cross( self, v ):
      '''
      produit vectorielle
      '''
      x = self._xyz[1] * v.xyz[2] - self._xyz[2] * v.xyz[1] 
      y = self._xyz[2] * v.xyz[0] - self._xyz[0] * v.xyz[2]
      z = self._xyz[0] * v.xyz[1] - self._xyz[1] * v.xyz[0]
      return Vector( (x, y, z) )
        
    @property
    def norm( self ):
      '''
      norme equivalent
      math.sqrt(np.sum(self._xyz**2)))
      '''
      sum2 = 0
      for i in range(3):
        sum2 += ( self._xyz[i] )**2
      return math.sqrt( sum2 )

    @property
    def normalisation( self ):
      '''
      '''
      n = self.norm
      a = [0, 0, 0]
      for i in range(3):
        a[i] = self._xyz[i] / n
      self._xyz = a
      return self
        
    def square_distance( self, p ):
      sum = 0.
      for i in range(3):
        sum += ( p._xyz[i] - self.xyz[i] )**2
      return sum
    
    def __add__(self, other):
      a = [0, 0, 0]
      for i in range(3):
        a[i] = self._xyz[i] + other.xyz[i]
      return Vector( a )

    def __iadd__(self, other):
      a = [0, 0, 0]
      for i in range(3):
        a[i] = self._xyz[i] + other.xyz[i]
      self._xyz = ( a )
      return self

    def __sub__(self, other):
      a = [0, 0, 0]
      for i in range(3):
        a[i] = self._xyz[i] - other.xyz[i]
      return Vector( a )

    def __isub__(self, other):
      a = [0, 0, 0]
      for i in range(3):
        a[i] = self._xyz[i] - other.xyz[i]
      self._xyz = ( a )
      return self
    
    def __mul__( self, other ):
      if isinstance( other, Vector ):
        return self.dot( other )
      return Vector( self._xyz[0] * other, self._xyz[1] * other, self._xyz[2] * other)
    
    def __eq__( self, other ):
      for i in range(3):
        if self._xyz[i] != other.xyz[i]:
          return False
      return True
        
    def eq( self, other, p = None ):
      coef = alu.usePrecision( p )
      if coef is None:
        for i in range(3):
          if self._xyz[i] != other.xyz[i]:
            return False
      for i in range(3):
        if not alu.eq( self._xyz[i], other.xyz[i], p):
          return False
      return True


if __name__ == '__main__':
  from ArithmeticLogic import ArithmeticLogic
  al = ArithmeticLogic( 2 )
  v = Vector( 1.2, 1.3, 1.0 )
  u = Vector( 2.1, 0.8, 1.3 )
  # produit vectoriel
  assert( al.eq( 4.86, v.dot(u) ) )
  assert( al.eq( 4.86, u.dot(v) ) )
  # norm
  assert( al.eq( 2.031, v.norm ) )
  assert( al.eq( 2.032241, v.norm, 5 ) )
  #
  alu = ArithmeticLogic( 2 )
  assert( Vector( 0.89, 0.54, -1.77 ).eq( v.cross( u ) ) )
  assert( Vector( -0.89, -0.54, 1.77 ).eq( u.cross( v ) ) )
  #
  assert( Vector( 0.9, -0.5, 0.3 ).eq( u-v, 2 ) )
  assert( Vector( 3.3, 2.1, 2.3 ).eq( u+v, 2 ) )
  #
  u -= v
  assert( Vector( 0.9, -0.5, 0.3 ).eq( u, 2 ) )
  u += v
  assert( Vector( 2.1, 0.8, 1.3 ).eq( u, 2 ) )
  u -= v
  assert( Vector( 0.9, -0.5, 0.3 ).eq( u, 2 ) )
  u += ( v + v )
  assert( Vector(3.3, 2.1, 2.3 ).eq( u, 2 ) )
  #
  a = Vector( 1, 2, 3 )
  b = Vector( 4, 5, 6 )
  assert( Vector( -3., 6., -3. ).eq( a.cross(b), 2 ) )
  assert( a.cross(b).eq( Vector(-3., 6., -3.), 2 ) )
  assert( b.cross(a).eq( Vector(3., -6., 3.), 2 ) )
  #
  c = ArithmeticLogic( 4 )
  u = Vector( 1., 1., 0. )
  assert( al.eq( math.sqrt( 2. ), u.norm ) )
  u = Vector( 1., 1., 1. )
  assert( al.eq( math.sqrt( 3. ), u.norm ) )
  #
  u = Vector( 1., 1., 0. )
  u.normalisation
  assert( Vector( 1./math.sqrt( 2. ), 1./math.sqrt( 2. ), 0.).eq( u, 4 ) )
  #
  u = Vector( 1., 1., 0. )
  v = Vector( -1., -1., 0. )
  assert( al.eq( 8., u.square_distance(v) ) )

