from ArithmeticLogic import alu

class Box:
    '''
    Geometric Box
    '''
    __slots__ = [ '_bounds' ]

    def __init__( self, bounds ):
        '''
        Constructor
        '''
        self._bounds = bounds
        
    def center( self ):
        return (self._bounds[0]+(self._bounds[1]-self._bounds[0])/2., self._bounds[2]+(self._bounds[3]-self._bounds[2])/2., self._bounds[4]+(self._bounds[5]-self._bounds[4])/2.)

    @property
    def bounds( self ):
        return self._bounds

    def points( self ):
        '''
        Generator 8 points
        '''
        yield (self._bounds[0], self._bounds[2], self._bounds[4])
        yield (self._bounds[1], self._bounds[2], self._bounds[4])
        yield (self._bounds[0], self._bounds[3], self._bounds[4])
        yield (self._bounds[1], self._bounds[3], self._bounds[4])
        yield (self._bounds[0], self._bounds[2], self._bounds[5])
        yield (self._bounds[1], self._bounds[2], self._bounds[5])
        yield (self._bounds[0], self._bounds[3], self._bounds[5])
        yield (self._bounds[1], self._bounds[3], self._bounds[5])

    def interieur(self, p ):
        if alu.between( self._bounds[0], p.xyz[0], self._bounds[1] ):
            if alu.between( self._bounds[2], p.xyz[1], self._bounds[3] ):
                if alu.between( self._bounds[4], p.xyz[2], self._bounds[5] ):
                    return True
        return False            

    def speedIntersecte( self, box ):
        '''
        Intersecte method with a align axis cell
        return -1, if out
        return 1, if in
        '''
        if box.bounds[0] > self._bounds[1] or box.bounds[1] < self._bounds[0]:
          return -1
        if box.bounds[2] > self._bounds[3] or box.bounds[3] < self._bounds[2]:
          return -1
        if box.bounds[4] > self._bounds[5] or box.bounds[5] < self._bounds[4]:
          return -1
        return 1

    def intersecte( self, box ):
        '''
        Intersecte method with a align axis cell
        return -1, if out
        return 0, if intersect
        return 1, if in
        
        '''
        if self.speedIntersecte( box ) == -1:
          return -1
        #
        for i in range(3):
          if box.bounds[2*i] < self._bounds[2*i]:
            return 0
          if box.bounds[2*i+1] > self._bounds[2*i+1]:
            return 0
        #
        return 1

if __name__ == '__main__':
    box = Box([-1.,1.,-1.,1.,-1.,1.])
    for p in box.points():
        print(p)

