from GeometricBox import Box

class Plane:
    '''
    Geometric Plane
    '''
    __slots__ = [ '_a', '_b', '_c', '_d' ]

    def __init__( self, a, b, c, d ):
        '''
        Constructor by cartesian equation
        a,b,c : pente pour chaque axe
        d : offset
        Ne permet pas de decrier un plan vertical ne passant pas par l'origine
        '''
        self._a = a
        self._b = b
        self._c = c
        self._d = d

    def __position( self, point ):
        d = self._a * point[0] + self._b * point[1] + self._c * point[2] + self._d
        if d > 0:
            return 1
        elif d < 0:
            return -1
        return 0

    def intersecte( self, box, mode='plane' ):
        '''
        Intersecte method with a align axis cell
        return -1, if up
        return 0, if intersect
        return 1, if down
        '''
        pos = None
        for point in box.points():
            if pos is None:
                pos = self.__position( point )
                if pos == 0:
                    return 0
            else:
                crtPos = self.__position( point )
                if crtPos == 0:
                    return 0
                if (pos > 0 and crtPos < 0) or (pos < 0 and crtPos > 0) :
                    return 0
        if mode == 'plane':
            return -1
        if mode == 'up':
            return pos
        return -pos                

